use indicatif::{ProgressBar, ProgressStyle};
use serde::Serialize;
use serde_json;
use std::env;
use std::fs;
use std::fs::File;
use std::io::{self, BufReader, Read, Write};
use std::sync::{mpsc, Arc, Mutex};
use std::thread;
use std::time::Instant;
use walkdir::WalkDir;

fn search_in_file(file_path: &str, search_string: &str) -> io::Result<bool> {
    let file = File::open(file_path)?;
    let mut buf_reader = BufReader::new(file);
    let mut contents = String::new();

    if buf_reader.read_to_string(&mut contents).is_err() {
        return Ok(false);
    }

    Ok(contents.contains(search_string))
}

fn get_total_files_count() -> u64 {
    WalkDir::new(".")
        .into_iter()
        .filter_map(Result::ok)
        .filter(|e| e.file_type().is_file())
        .count() as u64
}

fn setup_progress_bar(total: u64) -> ProgressBar {
    let pb = ProgressBar::new(total);
    let style_result = ProgressStyle::default_bar()
        .template("{spinner:.green} [{elapsed_precise}] [{bar:40.cyan/blue}] {pos}/{len} ({eta})");

    let style = style_result.unwrap_or_else(|e| {
        eprintln!("Error setting progress bar template: {}", e);
        ProgressStyle::default_bar()
    });

    pb.set_style(style);
    pb
}

#[derive(Serialize)]
struct PerfLog {
    commit_hash: String,
    num_files: u64,
    num_workers: usize,
    time_to_complete: u128, // milliseconds
}

fn main() -> io::Result<()> {
    let start_time = Instant::now();

    let search_string = match get_search_string_from_args(&env::args().collect()) {
        Some(s) => s,
        None => return Ok(()),
    };

    let total_files = get_total_files_count();
    let pb = Arc::new(Mutex::new(setup_progress_bar(total_files)));
    let n_workers = 12; // This will create 4 worker threads; you can adjust this number as needed
    let mut handles = vec![]; // To store thread handles

    let (tx, rx) = mpsc::channel::<String>();
    let rx = Arc::new(Mutex::new(rx));

    for _ in 0..n_workers {
        let search_string = search_string.clone();
        let pb = Arc::clone(&pb);
        let rx = Arc::clone(&rx);

        let handle = thread::spawn(move || {
            while let Ok(file_path) = rx.lock().unwrap().recv() {
                if search_in_file(&file_path, &search_string).unwrap_or(false) {
                    println!("Found in: {}", file_path);
                }
                pb.lock().unwrap().inc(1);
            }
        });

        handles.push(handle);
    }

    for entry in WalkDir::new(".")
        .into_iter()
        .filter_map(Result::ok)
        .filter(|e| e.file_type().is_file())
    {
        let file_path: String = entry.path().to_string_lossy().into_owned();
        tx.send(file_path).unwrap();
    }

    drop(tx); // Close the sending side of the channel

    for handle in handles {
        handle.join().unwrap();
    }

    pb.lock().unwrap().finish_with_message("Search completed!");

    // After your search is complete
    let duration = start_time.elapsed();

    let commit_hash = get_current_commit_hash().unwrap_or_else(|e| {
        eprintln!("Failed to get git commit hash: {}", e);
        "unknown".to_string()
    });
    // let num_files = /* however you're counting files */;
    let perf_log = PerfLog {
        commit_hash,
        num_files: total_files,
        num_workers: n_workers,
        time_to_complete: duration.as_millis(),
    };

    if let Err(e) = write_perf_log(perf_log) {
        eprintln!("Failed to write performance log: {}", e);
    }

    Ok(())
}

fn get_current_commit_hash() -> io::Result<String> {
    let output = std::process::Command::new("git")
        .args(&["rev-parse", "HEAD"])
        .output()?;
    Ok(String::from_utf8_lossy(&output.stdout).trim().to_string())
}

fn write_perf_log(log: PerfLog) -> io::Result<()> {
    let file = fs::OpenOptions::new()
        .write(true)
        .create(true)
        .append(true)
        .open("perfLogs.json")?;
    let mut writer = io::BufWriter::new(file);
    serde_json::to_writer(&mut writer, &log)?;
    writer.write_all(b"\n")?;
    Ok(())
}

fn get_search_string_from_args(args: &Vec<String>) -> Option<String> {
    if args.len() < 2 {
        println!("Usage: {} <search_string>", args[0]);
        None
    } else {
        Some(args[1..].join(" "))
    }
}

// ... (rest of the code remains the same)
#[cfg(test)]
mod tests {
    use super::*;

    use std::fs;

    #[test]
    fn test_search_in_file() -> io::Result<()> {
        fs::write("test_file.txt", "Hello, Rust!")?;
        assert!(search_in_file("test_file.txt", "Rust").unwrap());
        assert!(!search_in_file("test_file.txt", "Python").unwrap());
        fs::remove_file("test_file.txt")?;
        Ok(())
    }

    #[test]
    fn test_get_total_files_count() {
        let count = get_total_files_count();
        assert!(count > 0);
    }

    #[test]
    fn test_get_search_string_no_args() {
        let args = vec!["binary_name".to_string()];
        assert_eq!(get_search_string_from_args(&args), None);
    }

    #[test]
    fn test_get_search_string_with_args() {
        let args = vec![
            "binary_name".to_string(),
            "search".to_string(),
            "this".to_string(),
        ];
        assert_eq!(
            get_search_string_from_args(&args),
            Some("search this".to_string())
        );
    }
}
